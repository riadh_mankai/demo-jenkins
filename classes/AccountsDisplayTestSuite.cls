@isTest
private class AccountsDisplayTestSuite {
	@isTest
	static void controllerTestCase() {
		Account a = new Account (Name = 'test');
		Account b = new Account (Name = 'test');
		insert a;
		insert b;

		dvs_AccountsDisplayController controller = new dvs_AccountsDisplayController();
		System.assertEquals(controller.accounts.size(), [select Count() from Account]);
	}
}