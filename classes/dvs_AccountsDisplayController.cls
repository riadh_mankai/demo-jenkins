public with sharing class dvs_AccountsDisplayController {
	
	public List<Account> accounts {get; set;}
	
	public dvs_AccountsDisplayController() {
		accounts = new List<Account>();
		accounts = 
			[SELECT Id, Name
			FROM Account];
	}
}